#pragma once

#include <iostream>
#include <vector>
#include <algorithm>


struct MedianCalculator
{
	std::vector<int> process_array;

	void insert_int(int x) {
		process_array.push_back(x);
	}

	int calculate_median() {
		int size = process_array.size();
		if (size == 0) {
			std::cout << "Array is empty!" << std::endl;
			throw;
		}
		else {
			nth_element(process_array.begin(), process_array.begin() + 5, process_array.end());

			if (size % 2 == 0)
			{
				return (process_array[size / 2 - 1] + process_array[size / 2]) / 2;
			}
			else
			{
				return process_array[size / 2];
			}
		}
	}
};