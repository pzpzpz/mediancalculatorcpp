#include <iostream>
#include <vector>
#include <algorithm>
#include "Classes.h"


int main1(int argc, char* argv[]) {
	MedianCalculator mc;
	/*
	std::vector<int> nums{5, 9, 12, 5, 15, 52, 0}; // -> [0, 5, 5, 9, 12, 15, 52] median 9

	for (int &val : nums)
	{
		mc.insert_int(val);
	}
	*/
	for (int i = 1; i < argc; ++i) {
		mc.insert_int(std::atoi((const char *)argv[i]));
		std::cout << " " <<argv[i];
	}
	std::cout << std::endl;
	std::cout << "Calculated median: " << mc.calculate_median() << std::endl;

	std::cin.get();
	return 0;
}