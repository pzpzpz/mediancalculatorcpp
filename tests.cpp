#include <iostream>
#include <cstdlib>
#include <vector>
#include "Classes.h"

std::vector<int> generate_rand_vector(int len = 10) {
	std::vector<int> vect;
	for (int i = 0; i < len; i++) {
		vect.push_back(0 + rand() % static_cast<int>(100));
	}
	return vect;
}

int main() {
	for (int i = 0; i < 10; i++) {
		MedianCalculator mc;
		std::vector<int> v = generate_rand_vector();

		for (auto value : v) {
			mc.insert_int(value);
			std::cout << value << " ";
		}

		std::cout << "Calculated median: "<< mc.calculate_median() << std::endl;
	}
	std::cin.get();
}